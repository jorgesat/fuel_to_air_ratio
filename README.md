# Fuel-to-air ratio calculator

R shiny app to calculate the fuel-to-air ratio for the combustion of different gases.
Useful for mini-CAST and other flame burner soot generators.